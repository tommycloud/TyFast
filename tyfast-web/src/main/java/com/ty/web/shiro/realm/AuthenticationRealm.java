package com.ty.web.shiro.realm;

import com.google.common.collect.Sets;
import com.ty.api.log.service.LoginAuditLogService;
import com.ty.api.model.log.LoginAuditLog;
import com.ty.api.model.system.SysUser;
import com.ty.api.model.system.SysUserRole;
import com.ty.api.system.service.SysUserRoleService;
import com.ty.api.system.service.SysUserService;
import com.ty.cm.constant.ShiroConstant;
import com.ty.cm.utils.DataUtil;
import com.ty.cm.utils.StringUtil;
import com.ty.cm.utils.cache.Cache;
import com.ty.cm.utils.crypto.RSA;
import com.ty.web.shiro.AuthenticationToken;
import com.ty.web.utils.WebIpUtil;
import com.ty.web.utils.WebUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.ty.cm.constant.Messages.ERROR_MSG_ACCOUNT;
import static com.ty.cm.constant.Messages.ERROR_MSG_ACCOUNT_LOCKED;
import static com.ty.cm.constant.Messages.ERROR_MSG_ACCOUNT_NON_EXIST;
import static com.ty.cm.constant.Messages.ERROR_MSG_ACCOUNT_UNKNOWN;
import static com.ty.cm.constant.Messages.ERROR_MSG_EXCEPTION;
import static com.ty.cm.constant.Numbers.TWO;
import static com.ty.cm.constant.ShiroConstant.SESSION_TIMEOUT;
import static com.ty.cm.constant.ShiroConstant.STRATEGY_AUTHC;
import static com.ty.cm.constant.ShiroConstant.STRATEGY_URL;
import static com.ty.cm.constant.Ty.SLASH;

/**
 * 安全数据Realm
 *
 * 重要提示：
 * Shiro中 @Autowired 注入的类，一定要 再添加 @Lazy注解！
 *
 * 因为：Shiro框架初始化比Spring框架的某些部件早，导致使用@Autowire注入Shiro框架的某些类不能被Spring正确初始化
 * 会导致 事务失效等。
 *
 * @Author Tommy
 * @Date 2022/1/27
 */
@Slf4j
public abstract class AuthenticationRealm extends AuthorizingRealm {

    /** 账户业务接口 **/
    @Autowired
    @Lazy
    protected SysUserService sysUserService;

    /** 缓存对象 **/
    @Autowired
    @Lazy
    protected Cache cache;

    /**
     * 获取权限信息
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        return null;
    }
}
