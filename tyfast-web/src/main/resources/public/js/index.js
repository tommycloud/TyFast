// 初始化Vue
let app = new Vue({
  el: "#app",
  extends: baseApp,
  mounted() {
    this.$nextTick(() => {
      const flyline = new FlyLine("#map_container", "100000", {
        centerPoint: {name: '武汉', value: [114.298572, 30.584355]},
        excludePointIndex: 16,
        inverse: true,
        mcolor: '#2C5376'
      });
      flyline.render();
    })
  },
  methods: {
    zoom(flag) {
      console.log(flag? "放大":"还原");
    },
    refresh() {
      console.log("刷新面板...");
    }
  }
});