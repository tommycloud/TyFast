const defaultLang = 'zh_CN';
const vuetify = new Vuetify({
  lang: {
    current: defaultLang,
    t: (key, ...params) => i18n.t(key, params)
  }
});

/**
 * 引入Toast插件
 */
VuetifyMessageSnackbar.setVuetifyInstance(vuetify);
Vue.prototype.$message = VuetifyMessageSnackbar.Notify;

/*
 * 配置表单验证插件 VeeValidate i18N 国际化
 */
VeeValidate.configure({
  defaultMessage: function(field, values) {
    values._field_ = i18n.t(field);
    return i18n.t(`validations.messages.${values._rule_}`, values);
  }
});

/*
 * 扩展VeeValidate验证规则
 */
// 中文字符规则
VeeValidate.extend('chinese', {
  validate: value => {
    const reg = /^([\u4E00-\u9FA5\uF900-\uFA2D，。？！、；：【】“”‘’'']+)$/;
    return reg.test(value);
  }
});

// 仅包含字母、数字、下划线、破折号等规则
VeeValidate.extend('letter_dash', {
  validate: value => {
    const reg = /^([A-Za-z0-9_/\-]+)$/;
    return reg.test(value);
  }
});

// 同步验证的规则
VeeValidate.extend('check', {
  validate: (value, {func}) => {
    return app[func](value);
  },
  params: ['func']
});

// Ajax异步验证的规则
VeeValidate.extend('async', {
  validate: async (value, {url, prop, ref}) => {
    // 请求参数
    let param = {};
    param[prop] = value;

    // 清除当前的错误状态
    if (app.$refs[ref + 'VP']) {
      app.$refs[ref + 'VP'].errors=[];
    }
    app.$refs[ref].loading = true;

    // 发送请求，并等待响应结果
    let result = {};
    await doAjaxPost(ctx + url, param, (data) => {
      result = data;
    });
    app.$refs[ref].loading = false;
    return result.state? true : result.message;
  },
  params: ['url', 'prop', 'ref']
});

/**
 * VueI18n 插件
 * 官方文档：https://kazupon.github.io/vue-i18n/zh/started.html
 */
const i18n = new VueI18n({
  locale: defaultLang,          // 设置语言环境
  fallbackLocale: defaultLang,  // 预设的语言环境【当前语言环境没有要获取的值时，默认从这个语言环境查找（预设的语言环境·首选语言缺少翻译时要使用的语言）】
  messages: {},                 // 设置各本地化的语言包
  silentFallbackWarn: true,     // 是否在回退到 fallbackLocale 或 root 时取消警告（如果为 true，则仅在根本没有可用的转换时生成警告，而不是在回退时。）
  silentTranslationWarn: true,   // 是否取消本地化失败时输出的警告
  preserveDirectiveContent: true // 解决翻译内容闪烁的问题(有过渡动画时，可复现此问题)
});

/**
 * Vue APP基类：封装基础业务功能，以简化使用(基于 Vue extends 实现功能复用)
 */
const baseApp = {
  i18n,
  vuetify,
  directives: {
    blank: { //自定义指令：当文本框或文本域的值为null时，将null转换为空字符串
      bind: function(el) {
        let textEl = null;
        let tagName = el.tagName.toUpperCase();
        switch (tagName) {
          case "INPUT":
          case "TEXTAREA":
            textEl = el;
        }
        textEl = textEl || el.querySelector("input[type]");
        textEl = textEl || el.querySelector("textarea");
        textEl && textEl.addEventListener('blur', function () {
          if (this.value === '') {
            this.dispatchEvent(new Event('input'))
          }
        });
      }
    }
  },
  data: function() {
    return {
      fullscreenIcon: "mdi-fullscreen",
      isLinkWS: true, // 是否建立WebSocket连接
      sysNavStatus: true, // 左侧导航栏状态：显示/隐藏
      sysNavariantStatus: false, // 左侧导航栏是否只以图标形式显示
      sysNavariantWidth: 70, // 图标形式时，左侧导航栏的宽度
      menuName: '', // 页面上显示的菜单名称
      navMenus: [], // 导航菜单数据
      deactiveMenu: false, // 导航菜单项不高亮显示
      storageMenuKey: "navMenus", // 存储在LocalStorage中的菜单数据Key
      storageNavStatusKey: "navStatus", // 存储在LocalStorage中的导航菜单状态Key
      vtheme: 'light', // Vuetify主题
      storageThemeKey: 'vuetifyTheme', // 存储在LocalStorage中的主题数据Key
      langList: [],// 语言列表
      lang: null,  // 当前语言环境
      loading: false, // 数据加载状态
      posting: false, // 请求状态
      overlay: false, // 全屏Loading
      tabActivity: true, // 浏览器选项卡激活状态
      socketState: 0, // WebSocket连接状态: 0=初次连接中，1=掉线，9=连接成功
      pagination: { // 分页对象
        page: 1,
        pageSize: 20,
        totalPages: 0,
        vp: 10
      },
      dictConfig: {} // 数据字典配置
    }
  },
  computed: {
    // 项目根路径
    ctx() {
      const base = ctx || '/';
      return base.substr(0, base.length - 1);
    },
    // 是否为Dark Mode
    vdark() {
      return this.$vuetify.theme.dark;
    }
  },
  watch: {
    sysNavariantStatus(val) {
      localStorage.setItem(this.storageNavStatusKey, val);
    },
    lang(val) {
      $cookies.set("lang", val, '1y');
      this.$i18n.locale = val;
      this.$vuetify.lang.current = val;

      // 加载语言本地化资源包
      this.loadLangResources(this.changeLangCallback || (() => {this.datatable && this.datatable.headers && t(this.datatable.headers)}));
    },
    tabActivity(val) {
      if (val) {
        this.onActivityStarted && this.onActivityStarted();
      } else {
        this.onActivityStopped && this.onActivityStopped();
      }
    }
  },
  created() {
    // 移动端默认收起导航栏
    if (this.$vuetify.breakpoint.xs || this.$vuetify.breakpoint.sm) {
      this.sysNavStatus = false;
    }

    // 注册全屏事件监听器
    this.fullScreenListener();

    // 加载系统导航菜单
    this.getNavMenus();

    // 切换Vuetify主题
    this.switchTheme();

    // 读取查询参数
    this.param = readQueryParam(this.menuName, this.param);
    this.pagination.page = (this.param && this.param.page) || this.pagination.page;
  },
  mounted() {
    // 建立WebSocket通讯通道
    if (this.isLinkWS) {
      connect();
    }

    // 获取数据字典
    this.doQueryDicts();

    // 加载语言列表
    this.loadLangList();
  },
  methods: {
    // 浏览器全屏事件监听
    fullScreenListener() {
      screenfull && screenfull.onchange(() => {
        this.fullscreenIcon = screenfull.isFullscreen? "mdi-fullscreen-exit" : "mdi-fullscreen";
      });
    },

    // 浏览器全屏模式与普通模式切换
    toggleFullScreen() {
      screenfull.toggle();
    },

    // 切换导航菜单的显示方式
    toggleSysMenu() {
      if (this.sysNavStatus) {
        this.sysNavariantStatus = !this.sysNavariantStatus;
      } else {
        this.sysNavStatus = !this.sysNavStatus;
        this.sysNavariantStatus = false;
      }
    },

    // 滚动到顶部
    scrollTop(duration) {
      if (!this.method) {
        if (this.$vnode) {
          this.$el && this.$el.scroll(0,0);
        } else {
          this.$vuetify.goTo(0, {duration: duration || 0});
        }
      }
    },

    // 滚动到数据表格顶部
    scrollDTableTop(ref) {
      if (!(this.method === 'update' || this.method === 'del')) {
        ref = ref || 'dataTable';
        this.$refs[ref] && this.$refs[ref].$el.querySelector(".v-data-table__wrapper").scroll(0,0);
      }
    },

    // 返回上一页
    back() {
      window.history.back();
    },

    // URL函数：自动添加项目名称，并支持随机参数，解决静态缓存问题
    url(link, appendTail, tv) {
      if (link) {
        link = this.ctx + link;
      }

      if (appendTail) {
        let t = tv || new Date().getTime();
        if (typeof(link) === 'string') {
          link += (link.lastIndexOf("?") > -1? '&' : '?') + '_t=' + t;
        }
      }
      return link;
    },

    // 获取合适的分页页码
    getFitPage(pagination, datatable) {
      pagination = pagination || this.pagination;
      datatable = datatable || this.datatable;

      let page = pagination.page ?? 1;
      let items = datatable.items || [];
      if (page > 1 && this.method === 'del') {
        page = items.length <= 1? (page - 1) : page;
      }
      if (this.method === 'save') {
        page = 1;
      }
      this.method = null;
      return page;
    },

    // Toast
    toast(msg, msgType) {
      // 只在浏览器Tab页签被激活的情况下，才触发
      if (this.tabActivity) {
        msg = (undefined !== msg && null !== msg)? this.$t(msg + '') : msg;
        let toastObj = this.$message.topRight().closeButtonContent('x');
        switch (msgType) {
          case 'warning':
            toastObj.warning(msg);
            break;
          case 'error':
            toastObj.error(msg);
            break;
          default:
            toastObj.success(msg);
        }
      }
    },

    // Vee验证策略vchange: 当值变化时验证
    vchange(context) {
      return context.value != null? { on: ['change'] } : { on: ['blur'] };
    },

    // 合并属性值：将 Source 的属性值 复制到 Target (只复制Target存在的属性)
    mergeValue(target, source) {
      Object.keys(source).filter(p => p in target).forEach(p => target[p] = source[p])
    },

    // 将列表数据包装为树结构数据 (data 与 idKey 参数为必选)
    wrapTreeData(data, idKey, headers, parentKey, activeKey) {
      let datax = [];
      if (data instanceof Array && data.length > 0) {
        parentKey = parentKey || 'parentId';

        // 为不改变原始数据，所以将数据复制一份
        for (let item of data) {
          if (activeKey) {
            item[activeKey] = false;
          }
          datax.push({...item});
        }

        // 通过map 与 filter 进行数据变换
        let childIds = {}; // 记录所有子节点ID
        datax = datax.map((currentValue, index, arr) => {
          // 查找当前节点的所有子节点
          let children = [];
          for (let item of arr) {
            if (currentValue[idKey] === item[parentKey]) { // 若主外键相同
              children.push(item);
              childIds[item[idKey]] = item[idKey];
            }
          }
          if (children.length > 0) {
            currentValue.children = children;
          }
          return currentValue;
        }).filter((currentValue) => {
          // 过滤掉所有子节点（因为子节点已被添加到 父节点的 children 属性，若保留，则造成数据重复，也就不是树数据了）
          let flag = !childIds[currentValue[idKey]];
          if (flag) {
            // 通过递归计算各个节点的实际层级
            // 计算层级，主要用于在 TreeView组件中，让列数据对齐
            let updateNodeLevel = function(val) {
              if (val.children) {
                for (let i in val.children) {
                  let item = val.children[i];
                  item.level = val.level + 1;
                  updateNodeLevel(item);
                }
              }
            };
            currentValue.level = 1;
            updateNodeLevel(currentValue);
          }
          return flag;
        });
      }

      // 添加树表格的表头
      if (headers) {
        datax.unshift(headers);
      }
      return datax;
    },

    // 重置表单
    resetForm(form, observer) {
      form = form || 'dataForm';
      observer = observer || 'observer';

      if (this.$refs[form]) {
        let formDom = this.$refs[form];
        let formContainer = formDom.$el.querySelector('.v-card__text');
        formContainer && formContainer.scroll(0,0);
        formDom.reset();
      }

      if (this.$refs[observer]) {
        this.$refs[observer].reset();
      }
    },

    // 注销登录
    logout() {
      sessionStorage.clear();
      localStorage.clear();
      this.vtheme && localStorage.setItem(this.storageThemeKey, this.vtheme);
      window.location.href = this.url("/logout");
    },

    // 打开个人设置
    openUserSetting() {
      window.location.href = this.url("/user/profile/view")
    },

    // 获取系统菜单数据
    getNavMenus(callback) {
      // 状态
      let sysNavariantStatus = localStorage.getItem(this.storageNavStatusKey);
      if (sysNavariantStatus) {
        this.sysNavariantStatus = !("false" === sysNavariantStatus);
      }

      // 数据
      let navMenusJson = localStorage.getItem(this.storageMenuKey);
      if (navMenusJson) {
        this.navMenus = JSON.parse(navMenusJson);
        this.activeNavMenuItem();
      } else {
        doAjax(this.url("/system/menu/user/list"), null, data => {
          if (data.state) {
            // 将菜单URL，添加项目路径前缀
            data.data = data.data || [];
            data.data.filter(item => {
              if (item.url) {
                item.url = item.url.startsWith("http")? item.url : this.url(item.url);
              }
            });

            // 构建菜单树
            this.navMenus = this.wrapTreeData(data.data, 'menuId', null, null, "selected");
            this.navMenus.unshift({menuId: "1",menuName: "首页",icon: "mdi-home",url: this.url("/index"),selected: false}); // 添加"首页"菜单项
            this.activeNavMenuItem();
            localStorage.setItem(this.storageMenuKey, JSON.stringify(this.navMenus));

            // 回调函数
            if (callback) {
              callback(this.navMenus);
            }
          } else {
            this.toast('获取左侧导航菜单失败，请稍后重试!', 'warning');
          }
        });
      }
    },

    // 高亮显示导航菜单项
    activeNavMenuItem(item) {
      item = item || {url: location.pathname};
      let currentItem = null, parentItem = null, prevItem = null, prevParentItem = null;
      for (let m of this.navMenus) {
        // 没有子菜单的情况
        if (m.url && m.url === item.url) {
          currentItem = m
        } else {
          prevItem = m.selected? m : prevItem;
          m.selected = false;
        }

        // 有子菜单的情况
        if (m.children) {
          for (let c of m.children) {
            if (c.url && c.url === item.url) {
              currentItem = c;
              parentItem = m;
            } else {
              if (c.selected) {
                prevItem = c;
                prevParentItem = m;
              }
              c.selected = false;
            }
          }
        }
      }

      // Active Menu
      if (currentItem) {
        currentItem && (currentItem.selected = true);
        parentItem && (parentItem.selected = true);
      } else {
        if (!this.deactiveMenu) {
          prevItem && (prevItem.selected = true);
          prevParentItem && (prevParentItem.selected = true);
        }
      }
    },

    // 重载系统菜单数据
    reloadNavMenus(callback) {
      localStorage.removeItem(this.storageMenuKey);
      this.getNavMenus(callback);
    },

    // 打开目标页面
    gotoPage(item, e) {
      clearQueryParam();
      this.activeNavMenuItem(item);
      localStorage.setItem(this.storageMenuKey, JSON.stringify(this.navMenus));

      // 跳转到目标页
      if ('_self' === (item.target??'_self')) {
        e.preventDefault();
        this.overlay = true;
        window.location.href = item.url;
      }
    },

    // 获取数据字典值
    doQueryDicts(callback, config) {
      let codes = Object.keys(config || this.dictConfig).join(",");
      if (codes.length > 0) {
        doAjaxPost(this.url("/system/dict/items"), {codes}, (data) => {
          let dataMap = data.data;
          if (callback) {
            callback(dataMap || {});
          } else {
            if (dataMap) {
              for (let p in this.dictConfig) {
                let items = dataMap[p];
                if (items) {
                  this[this.dictConfig[p]] = items;
                }
              }
            }
          }
        });
      }
    },

    // Vuetify主题切换
    switchTheme(val) {
      if (val) {
        localStorage.setItem(this.storageThemeKey, val);
      } else {
        this.vtheme = localStorage.getItem(this.storageThemeKey) || 'dark';
      }
      this.$vuetify.theme.dark = this.vtheme === 'dark';
      this.$nextTick(() => {
        this.themeEvent && this.themeEvent();
        let bodyClasslist = document.querySelector("body").classList;
        if (this.$vuetify.theme.dark) {
          bodyClasslist.add('theme--dark');
        } else {
          bodyClasslist.remove('theme--dark');
        }
      });
    },

    // 加载语言列表
    loadLangList() {
      doAjaxGet(this.url("/lang/list"), null, result => {
        this.langList = result.data || [];
      });

      // 设置当前语言环境
      this.lang = $cookies.get("lang") || defaultLang;
    },

    // 加载语言本地化资源
    loadLangResources(callback) {
      let messages = i18n.messages[this.lang];
      if (!messages) { // 若不存在静态语言包，则加载
        loadJScript(this.url("/assets/lang/" + this.lang + ".js", true, _v), callback);
      } else {
        callback && callback();
      }
    }
  }
};

/**
 * 浏览器选项卡显隐监听事件
 */
document.addEventListener("visibilitychange", function () {
  app && (app.tabActivity = !(document.hidden === true));
});
window.addEventListener('pagehide', function (event) {
  app && (app.tabActivity = event.persisted? true : false);
});